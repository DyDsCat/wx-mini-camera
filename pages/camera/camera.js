

Page({
  takePhoto() {
    const ctx = wx.createCameraContext()
    ctx.takePhoto({
      quality: 'high',
      success: (res) => {
        this.setData({
          src: res.tempImagePath
        })
      }
    })
    wx.request({
      url: 'http://localhost:8099/',
      success: (res) => {
        this.setData({
          result: res.data
        })
        console.log(res);
      }
    })
  },
  chooseImage() {
    wx.chooseMedia({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album'],
      success: (res) => {
        this.uploadImage(res.tempImagePath);
      }
    })
  },
  uploadImage(imagePath) {
    wx.request({
      url: 'http://localhost:8099/',
      success: (res) => {
        this.setData({
          result: res.data
        })
        console.log(res);
      }
    })
  },
  error(e) {
    console.log(e.detail)
  }
})